package dk.kea.swd.imcrypt.client.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class Network {
    private static final String serverHostAddress = "localhost";
    private static final int serverPort = 7324;
    private static Socket mySocket;

    private static void ensureConnection() throws Exception {
        if (mySocket == null)
            mySocket = new Socket(serverHostAddress, serverPort);
    }

    public static void sendToServer(String message) throws Exception {
        Network.ensureConnection();

        DataOutputStream outToServer = new DataOutputStream(mySocket.getOutputStream());
        outToServer.writeBytes(message + '\n');

        System.out.println("Sent: " + message);
    }

    public static String receiveFromServer() throws Exception {
        Network.ensureConnection();

        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
        String message = inFromServer.readLine();

        System.out.println("Received: " + message);
        return message;
    }
}