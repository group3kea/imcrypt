package dk.kea.swd.imcrypt.client.controller;

public interface MyRunnable {
    void run() throws Exception;
}