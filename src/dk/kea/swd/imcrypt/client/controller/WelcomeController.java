package dk.kea.swd.imcrypt.client.controller;

import dk.kea.swd.imcrypt.client.network.Network;
import dk.kea.swd.imcrypt.client.view.ui.MyAlert;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;

public class WelcomeController implements MyController {

    @FXML
    private TextField nameField;
    private MainApp mainApp;

    @FXML
    private void onStartClicked() {
        mainApp.runInParallel(() -> {
            Network.sendToServer("Server " + nameField.getText());

            String response = Network.receiveFromServer();

            while (!validateResponse(response))
                response = Network.receiveFromServer();

            processPacket(response);
        });
    }

    private boolean validateResponse(String response) {
        return response.equals("Success") || response.equals("Name Unavailable");
    }

    private void processPacket(String response) {
        if (response.equals("Success")) {
            mainApp.showPage("NewSession");
        } else if (response.equals("Name Unavailable")) {
            MyAlert.show(AlertType.WARNING,
                    "Name Unavailable",
                    "There is already a player with the same name on the server",
                    "Try again with another name");
        }
    }

    @Override
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
}