package dk.kea.swd.imcrypt.client.controller;

import dk.kea.swd.imcrypt.client.network.Network;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

public class ChatRoomController implements Initializable, MyController {

    @FXML
    private TextArea chatBox;
    @FXML
    private TextField messageBox;
    @FXML
    private Label receiverName;
    private MainApp mainApp;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        chatBox.clear();
    }

    public void start() {
        receiverName.setText(mainApp.getReceiverName());
        if (mainApp.getChatStatus().equals("Wait for receiver")) {
            mainApp.runInParallel(() -> {
                String response = Network.receiveFromServer();

                while (!validateResponse(response)) {
                    response = Network.receiveFromServer();
                }

                processPacket(response);
            });
        }
    }

    @FXML
    private void onSendClicked() {
        mainApp.runInParallel(() -> {
            Network.sendToServer(mainApp.getReceiverName() + " " + messageBox.getText());
            messageBox.clear();

            String response = Network.receiveFromServer();

            while (!validateResponse(response)) {
                response = Network.receiveFromServer();
            }

            processPacket(response);
        });
    }

    private boolean validateResponse(String response) {
        StringTokenizer splittedMessage = new StringTokenizer(response);
        return splittedMessage.nextToken().equals(mainApp.getReceiverName());
    }

    private void processPacket(String response) {
        StringTokenizer splittedMessage = new StringTokenizer(response);
        String messageToDisplay = "";

        splittedMessage.nextToken();
        while (splittedMessage.hasMoreTokens()) {
            messageToDisplay += " ";
            messageToDisplay += splittedMessage.nextToken();
        }
        messageToDisplay += '\n';

        chatBox.setText(chatBox.getText() + messageToDisplay);
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

}