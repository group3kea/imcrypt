package dk.kea.swd.imcrypt.client.controller;

import dk.kea.swd.imcrypt.client.view.ui.MyAlert;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class MainApp extends Application {

    private static BorderPane myLayout;
    private String receiverName;
    private String chatStatus;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane rootLayout = FXMLLoader.load(MainApp.class.getResource("/dk/kea/swd/imcrypt/client//view/RootLayout.fxml"));
        primaryStage.setMinWidth(1000);
        primaryStage.setMinHeight(1000);
        primaryStage.setTitle("IMCrypt");
        primaryStage.setScene(new Scene(rootLayout));
        primaryStage.show();
        myLayout = rootLayout;

        showPage("Welcome");
    }

    public void showPage(String name) {
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/dk/kea/swd/imcrypt/client/view/" + name + ".fxml"));
            Pane page = loader.load();
            MyController controller = loader.getController();
            controller.setMainApp(this);
            if (controller instanceof ChatRoomController) {
                ((ChatRoomController) controller).start();
            }
            myLayout.setCenter(page);
        } catch (Exception e) {
            MyAlert.show(AlertType.ERROR, "Application files are corrupted",
                    e.getMessage(),
                    "The app will now close");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void handleServerException(Exception e) {
        Platform.runLater(() -> {
            MyAlert.show(AlertType.ERROR, "Server error",
                    e.getMessage(),
                    "The app will now close");
            e.printStackTrace();
            System.exit(-1);
        });
    }

    public void runInParallel(MyRunnable block) {
        try {
            block.run();
        } catch (Exception e) {
            handleServerException(e);
        }
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getChatStatus() {
        return chatStatus;
    }

    public void setChatStatus(String chatStatus) {
        this.chatStatus = chatStatus;
    }
}