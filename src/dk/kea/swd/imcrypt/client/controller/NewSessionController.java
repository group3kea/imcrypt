package dk.kea.swd.imcrypt.client.controller;

import dk.kea.swd.imcrypt.client.network.Network;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class NewSessionController implements MyController {

    @FXML
    private TextField nameField;
    private MainApp mainApp;
    private boolean inviteClicked;

    @FXML
    private void onWaitClicked() {
        inviteClicked = false;
        mainApp.runInParallel(() -> {
            String response = Network.receiveFromServer();
            processPacket(response);
        });
    }

    @FXML
    private void onInviteClicked() {
        inviteClicked = true;
        mainApp.runInParallel(() -> {
            Network.sendToServer(nameField.getText());

            mainApp.setReceiverName(nameField.getText());
            mainApp.setChatStatus("Send message");
            mainApp.showPage("ChatRoom");
        });
    }

    private void processPacket(String response) {
        if (!inviteClicked) {
            mainApp.setReceiverName(response);
            mainApp.setChatStatus("Wait for receiver");
            mainApp.showPage("ChatRoom");
        }
    }

    public void setMainApp(MainApp app) {
        this.mainApp = app;
    }
}
