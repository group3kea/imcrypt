package dk.kea.swd.imcrypt.client.view.ui;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;

/**
 * Created by Daniel Constantin on 10-Sep-15.
 */
public class MyAlert {

    /**
     * Displays an alert of the given type with the given parameters.
     */
    public static void show(AlertType type, String title, String headerText, String contentText) {
        Alert alert = new Alert(type);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }
}
