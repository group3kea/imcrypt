package dk.kea.swd.imcrypt.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Server {

    private static ServerSocket serverSocket;
    private static Map<String, Socket> clientMap;

    public static void startServer(int port) throws IOException {
        clientMap = new HashMap<>();
        serverSocket = new ServerSocket(port);
    }

    public static void main(String[] args) throws IOException {
        startServer(7324);

        while (true) {
            try {
                Socket clientSocket = serverSocket.accept();
                new Thread(() -> {
                    String clientName = null;
                    try {
                        while (true) {
                            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                            String message = inFromClient.readLine();
                            System.out.println("Received:" + message);
                            StringTokenizer splittedMessage = new StringTokenizer(message);

                            String messageToSend;
                            Socket receiverSocket;

                            String receiverName = splittedMessage.nextToken();

                            if (receiverName.equals("Server")) {
                                clientName = splittedMessage.nextToken();

                                if (clientMap.get(clientName) == null || clientMap.get(clientName).isClosed()) {
                                    clientMap.put(clientName, clientSocket);
                                    messageToSend = "Success" + '\n';
                                } else {
                                    messageToSend = "Name Unavailable" + '\n';
                                }
                                receiverSocket = clientSocket;

                            } else {
                                receiverSocket = clientMap.get(receiverName);

                                messageToSend = clientName;
                                while (splittedMessage.hasMoreTokens()) {
                                    messageToSend += " ";
                                    messageToSend += splittedMessage.nextToken();
                                }
                                messageToSend += '\n';
                            }

                            DataOutputStream outToClient = new DataOutputStream(receiverSocket.getOutputStream());
                            outToClient.writeBytes(messageToSend);

                            System.out.println("Sent:" + messageToSend);
                            System.out.println();
                        }
                    } catch (IOException e) {
                        clientMap.remove(clientName);
                        System.out.println(e.getMessage() + " for: " + clientName);
                        System.out.println("Nr of users: " + clientMap.size());
                        System.out.println();
                    }
                }).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}